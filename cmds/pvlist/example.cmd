require iocmetadata,0.1.0

epicsEnvSet(PREFIX, "EXAMPLE")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-A:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-B:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-C:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-D:")

pvlistFromInfo("ARCHIVE_THIS", "$(PREFIX):PvList1")
pvlistFromInfo("SAVERESTORE_THIS", "$(PREFIX):PvList2")

iocInit()
