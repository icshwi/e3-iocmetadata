# e3-iocmetadata  
ESS Site-specific EPICS module : iocmetadata (wrapper of the [iocMetadata](https://gitlab.esss.lu.se/icshwi/iocmetadata.git) module)

## Summary

This module will scan the record names of its IOC searching for specific info tags. It will then create a NTTable PV that contains the name of these records in its value. It is a way to expose the info tags of an IOC using PVAccess. The detailed explanation is in the [README](https://gitlab.esss.lu.se/icshwi/iocmetadata/-/blob/master/README.md) file of the main EPICS repository.

## Example of use case

This module can be used, for instance, as a helper to create a list of the IOC PVs that should be archived or added the a Save & Restore list. 

Given that all the records that contains PVs that should be archived also contain an specific info tag, like the example below:

```
example.template

record(ai, "$(PREFIX)SignalA") {
    field(DESC, "Simple analog input")
    info(ARCHIVE_THIS, "VAL EGU")
}

record(ai, "$(PREFIX)SignalB") {
    field(DESC, "Simple analog input")
    info(SAVERESTORE_THIS, "")
}
```
When the following IOC is executed:

```
require iocmetadata

epicsEnvSet(PREFIX, "EXAMPLE")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-A:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-B:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-C:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-D:")

pvlistFromInfo("ARCHIVE_THIS", "$(PREFIX):ArchiverList")
pvlistFromInfo("SAVERESTORE_THIS", "$(PREFIX):SaveRestoreList")

iocInit()
```
Two special NTTable PVs will be created:
- `EXAMPLE:ArchiverList`
- `EXAMPLE:SaveRestoreList`

Any PVA client can access the value of these PVs:
```
$> pvget EXAMPLE:ArchiverList
EXAMPLE:ArchiverList epics:nt/NTTable:1.0
    string[] pvlist [EXAMPLE-A:SignalA.VAL, EXAMPLE-A:SignalA.EGU, EXAMPLE-B:SignalA.VAL, EXAMPLE-B:SignalA.EGU, EXAMPLE-C:SignalA.VAL, EXAMPLE-C:SignalA.EGU, EXAMPLE-D:SignalA.VAL, EXAMPLE-D:SignalA.EGU]

$> pvget EXAMPLE:SaveRestoreList
EXAMPLE:SaveRestoreList epics:nt/NTTable:1.0
    string[] pvlist [EXAMPLE-A:SignalB, EXAMPLE-B:SignalB, EXAMPLE-C:SignalB, EXAMPLE-D:SignalB]
```

One can also use the `pvlistget.py` script to obtain a plain list of the values: (depends on [p4p](https://mdavidsaver.github.io/p4p/))

```
$> python3 pvlistget.py EXAMPLE:ArchiverList
EXAMPLE-A:SignalA.VAL
EXAMPLE-A:SignalA.EGU
EXAMPLE-B:SignalA.VAL
EXAMPLE-B:SignalA.EGU
EXAMPLE-C:SignalA.VAL
EXAMPLE-C:SignalA.EGU
EXAMPLE-D:SignalA.VAL
EXAMPLE-D:SignalA.EGU

$> python3 pvlistget.py EXAMPLE:SaveRestoreList
EXAMPLE-A:SignalB
EXAMPLE-B:SignalB
EXAMPLE-C:SignalB
EXAMPLE-D:SignalB
```

### References
- iocMetadata: https://gitlab.esss.lu.se/icshwi/iocmetadata.git
- p4p: https://mdavidsaver.github.io/p4p

